#pragma once

#include <llvm/IR/LLVMContext.h>
#include <llvm/IR/IRBuilder.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/GlobalValue.h>
#include <llvm/Support/raw_ostream.h>
#include <llvm/Support/TargetSelect.h>
#include <llvm/IR/LegacyPassManager.h>
#include <llvm/IR/PassManager.h>
#include <llvm/Passes/PassBuilder.h>
#include <llvm/Transforms/IPO/PassManagerBuilder.h>
#include <llvm/Support/TargetRegistry.h>
#include <string>
#include <llvm/Target/TargetOptions.h>
#include <llvm/Target/TargetMachine.h>
#include <llvm/Support/CodeGen.h>
#include <llvm/IR/Verifier.h>
#include <llvm/Support/Host.h>
#include <memory>
#include <ostream>

namespace lollang {
  void init();

  std::unique_ptr<llvm::TargetMachine> get_default_target();

  enum class AsmFormat {
    TEXT,
    OBJ
  };

  class LolLangCompiler {
  public:
    llvm::LLVMContext llvm_ctx;
    llvm::IRBuilder<llvm::ConstantFolder, llvm::IRBuilderDefaultInserter> irb;
    llvm::Module mod;
    std::unique_ptr<llvm::TargetMachine> target_machine;

    template<typename StrRef>
    explicit
    LolLangCompiler(StrRef program_name, std::unique_ptr<llvm::TargetMachine> target_machine = get_default_target())  :
            llvm_ctx(), irb(this->llvm_ctx), mod(program_name, this->llvm_ctx), target_machine(std::move(target_machine)) {
      this->mod.setDataLayout(this->target_machine->createDataLayout());
      this->mod.setTargetTriple(this->target_machine->getTargetTriple().getTriple());
    }

    void doOptimizations(llvm::PassBuilder::OptimizationLevel);

    void generateAsm(AsmFormat fmt, std::ostream &out);
  };
}

