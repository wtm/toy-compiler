#include "read_source.h"

using namespace std;
using namespace lollang;

string lollang::read_all(const char* path) {
  fstream fs(path, fstream::ate | fstream::binary | fstream::in);
  fs.exceptions(fstream::failbit | iostream::badbit);
  auto len = fs.tellg();
  string buf(len, '\0');
  fs.seekg(0, fstream::beg);
  fs.read(buf.data(), len);
  return buf;
}

vector<Word> lollang::lex(string_view str) {
  vector<Word> words;
  while (!str.empty()) {
    char c = str[0];
    if (c == ' ' || c == '\n' || c == '\t') {
      str = str.substr(1);
      continue;
    }
    if ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z')) {
      // todo
    }
  }
  return words;
}
