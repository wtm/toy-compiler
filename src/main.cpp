#include <iostream>
#include <llvm/Passes/PassBuilder.h>
#include "lollang.h"
#include "read_source.h"

using namespace std;
using namespace lollang;

int main() {
  cout << read_all("src/main.cpp");
//  init();
//  LolLangCompiler llc("main.lollang");
//  auto& irb = llc.irb;
//  auto& program = llc.mod;
//  llc.doOptimizations(llvm::PassBuilder::OptimizationLevel::O3);
//  llc.generateAsm(AsmFormat::TEXT, cout);
  return 0;
}

