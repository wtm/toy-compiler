#include "lollang.h"

#include <stdexcept>
#include <llvm/Support/raw_os_ostream.h>

using namespace llvm;
using namespace lollang;
using namespace std;

void lollang::init() {
  InitializeNativeTarget();
  InitializeNativeTargetAsmPrinter();
}

unique_ptr<TargetMachine> lollang::get_default_target() {
  auto default_target_str = sys::getDefaultTargetTriple();
  string error;
  auto default_target = TargetRegistry::lookupTarget(default_target_str, error);
  if (!default_target) {
    throw runtime_error(error);
  }
  TargetOptions target_opts;
  auto rm = Optional<Reloc::Model>();
  auto target_machine = default_target->createTargetMachine(default_target_str, "generic", "", target_opts, rm);
  return unique_ptr<TargetMachine>(target_machine);
}

void lollang::LolLangCompiler::doOptimizations(PassBuilder::OptimizationLevel opt_level) {
  PassBuilder pb;
  ModuleAnalysisManager mam;
  CGSCCAnalysisManager cam;
  FunctionAnalysisManager fam;
  LoopAnalysisManager lam;
  auto mpm = pb.buildPerModuleDefaultPipeline(opt_level);
  auto fpm = pb.buildFunctionSimplificationPipeline(PassBuilder::OptimizationLevel::O3,
                                                    PassBuilder::ThinLTOPhase::None);
  pb.crossRegisterProxies(lam, fam, cam, mam);
  cam.registerPass([&] { return FunctionAnalysisManagerCGSCCProxy(); });
  pb.registerModuleAnalyses(mam);
  pb.registerFunctionAnalyses(fam);
  pb.registerLoopAnalyses(lam);
  cam.registerPass([&] { return PassInstrumentationAnalysis(); });
  for (Function &fun : this->mod) {
    if (!fun.hasExactDefinition()) {
      continue;
    }
    if (verifyFunction(fun)) {
      string s;
      s.append("There are llvm errors in function ");
      s.append(string_view(fun.getName()));
      s.push_back('.');
      throw runtime_error(s);
    }
    while (true) {
      auto ps = fpm.run(fun, fam);
      if (ps.areAllPreserved()) {
        break;
      } else {
        fam.invalidate(fun, ps);
      }
    }
  }
  if (verifyModule(this->mod)) {
    throw runtime_error("There are llvm errors in module.");
  }
  auto ps = mpm.run(this->mod, mam);
//  mam.invalidate(this->mod, ps);
  lam.clear();
  fam.clear();
  cam.clear();
  mam.clear();
}

void LolLangCompiler::generateAsm(AsmFormat fmt, ostream& out) {
  auto ross = raw_os_ostream(out);
  auto bs = buffer_ostream((raw_ostream&) ross);
  legacy::PassManager pm;
  this->target_machine->addPassesToEmitFile(pm, bs, nullptr, fmt == AsmFormat::TEXT ? CGFT_AssemblyFile : CGFT_ObjectFile);
  pm.run(this->mod);
}

