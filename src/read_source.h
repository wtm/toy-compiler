#pragma once
#include <string>
#include <fstream>
#include <vector>

namespace lollang {
  using namespace std;

  string read_all(const char* path);

  class InputPair {
  public:
    // In bytes
    size_t start;
    // In bytes
    size_t end;

    inline InputPair(size_t start, size_t end) : start(start), end(end) {}
    inline string_view read(const string& input) const {
      if (this->start > this->end) {
        throw invalid_argument("start > end.");
      }
      if (this->end > input.size()) {
        throw out_of_range("Index out of range.");
      }
      return basic_string_view(&input[this->start], this->end - this->start);
    }
  };

  class Word {
  public:
    InputPair pair;
  };

  vector<Word> lex(string_view str);
}
